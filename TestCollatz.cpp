// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(10, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 10);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(200, 100));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 200);
    ASSERT_EQ(j, 100);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(210, 201));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 210);
    ASSERT_EQ(j, 201);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(1000, 900));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1000);
    ASSERT_EQ(j, 900);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 999999));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 999999);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(2000, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 2000);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 182);
}

TEST(CollatzFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(1000, 2000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1000);
    ASSERT_EQ(j, 2000);
    ASSERT_EQ(v, 182);
}

TEST(CollatzFixture, eval7) {
    tuple<int, int, int> t = collatz_eval(make_pair(5000, 10000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 5000);
    ASSERT_EQ(j, 10000);
    ASSERT_EQ(v, 262);
}

TEST(CollatzFixture, eval8) {
    tuple<int, int, int> t = collatz_eval(make_pair(1200, 99900));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1200);
    ASSERT_EQ(j, 99900);
    ASSERT_EQ(v, 351);
}

TEST(CollatzFixture, eval9) {
    tuple<int, int, int> t = collatz_eval(make_pair(999999, 999999));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 999999);
    ASSERT_EQ(j, 999999);
    ASSERT_EQ(v, 259);
}

TEST(CollatzFixture, eval10) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 1);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}
